# SP Templates

Premade HTML templates for kickstarting a new web application. Uses `encode-html-template-tag` under the hood.

## Dependencies
 - css.escape: ^1.5.1
 - encode-html-template-tag: ^2.4.0
## Modules

<dl>
<dt><a href="#module_sp-templates">sp-templates</a></dt>
<dd></dd>
</dl>

## Members

<dl>
<dt><del><a href="#tag">tag</a></del></dt>
<dd></dd>
</dl>

<a name="module_sp-templates"></a>

## sp-templates

* [sp-templates](#module_sp-templates)
    * [.page(options, body)](#module_sp-templates.page)
    * [.time(options, date)](#module_sp-templates.time)

<a name="module_sp-templates.page"></a>

### sp-templates.page(options, body)
Create a basic page with header, nav, main and footer.

**Kind**: static method of [<code>sp-templates</code>](#module_sp-templates)  

| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Page options |
| options.title | <code>String</code> | Page title |
| options.colour | <code>String</code> | Theme colour |
| body | <code>String</code> \| <code>Html</code> | The body |

<a name="module_sp-templates.time"></a>

### sp-templates.time(options, date)
Create an HTML element representing a date and time

**Kind**: static method of [<code>sp-templates</code>](#module_sp-templates)  

| Param | Type | Description |
| --- | --- | --- |
| options | <code>Object</code> | Extra attributes to add to the time component |
| options.lang | <code>String</code> | The locale to render the text in |
| date | <code>Date</code> | The datetime to use |

<a name="tag"></a>

## ~~tag~~
***Deprecated***

**Kind**: global variable  
