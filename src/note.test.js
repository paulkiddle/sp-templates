import note from '../src/note.js';

test('Renders full note', async()=>{
	expect(await note({
		author: {
			url: 'http://example.com',
			name: 'Paul',
			avatar: 'img.jpg'
		},
		date: '2021-06-25',
		footer: 'footer content'
	}, 'Note content').render()).toMatchSnapshot();
});

test('Renders light note', async()=>{
	expect(await note({
		author: 'Paul',
	}, 'Note content').render()).toMatchSnapshot();
});
