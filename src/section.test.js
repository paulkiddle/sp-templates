import section from '../src/section.js';

test('Section', async()=> {
	const s = section('Title', 'Body');
	expect(await s({id:'s1'}).render()).toMatchSnapshot();
	expect(await s().render()).toMatchSnapshot();
});
