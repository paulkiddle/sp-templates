import html, { element, flatt } from 'encode-html-template-tag';

const cast = (props, author) => {
	if(!author) {
		author = props;
		props = {};
	}

	return [ props, author ];
};

const hCard = (props, author) => {
	[props, author] = cast(props, author);

	const { rel, imgWidth, ...rest } = props;

	return element(
		'address',
		{
			itemscope: true,
			itemtype: 'https://microformats.org/wiki/h-card',
			...rest
		},
		html`<p itemprop="name" class="p-name" style="display:contents">
			${author.url ? element('a', { rel, itemprop: 'url',  class:'u-url', href: author.url }, author.name) : (author.name || author)}
			${author.avatar ? element('img', { class: 'u-photo', itemprop: 'photo', width: imgWidth, alt:'', src: author.avatar}) : ''}
		</p>`
	);
};

export default hCard;
export const author = (props, author) => {
	[props, author] = cast(props, author);

	const { class: cls, ...rest } = props;

	return hCard(
		{
			rel: 'author',
			class: flatt('p-author', cls),
			itemprop: 'author',
			...rest
		},
		author
	);
};

hCard.author = author;
