import { page ,tag ,time ,user, wrapper} from '../src/index.js';

test('Templates', async ()=>{
	for(const tpl of [user, wrapper]) {
		expect(await tpl({}, 'body').render()).toMatchSnapshot();
	}
});

test('page template', async ()=>{
	expect(await page({title:'title', head:'link'}, 'body').render()).toMatchSnapshot();
});

test('page template with footer', async ()=>{
	expect(await page({title:'title', head:'link', footer: 'foot'}, 'body').render()).toMatchSnapshot();
});

test('tag fn', async()=>{
	expect(await tag('el', {prop1:'val', prop2:null}, 'body').render()).toMatchSnapshot();
});

test('time tpl', async()=>{
	expect(await time(new Date('2020')).render()).toMatchSnapshot();
	expect(await time({locale: 'fr-FR'}, new Date('2020')).render()).toMatchSnapshot();
});
