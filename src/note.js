import html from 'encode-html-template-tag';
import time from './time.js';
import css from './css.js';
import hCard from './h-card.js';
import hEntry from './h-entry.js';

const width = '150px';

export const stylesheet = css`
.Note {
  display: grid;
  grid-template-areas:
    "header body"
    "header footer";
  grid-template-columns: ${width} auto;
	grid-template-rows:
		1fr auto;
}

.Note__header {
  grid-area: header;
}

.Note__author {
  display: flex;
  flex-direction: column
}
`;

export default ({author, date, footer, class: cls, ...attrs}, body) => html`
	${stylesheet}
	${hEntry(
		{
			class: ['Note', cls],
			...attrs
		},
		content => html`
			<header class="Note__header">
				${hCard.author({class: 'Note_author', imgWidth:width}, author)}
				${date ? time.published(date) : ''}
			</header>
			${content(body)}
			${footer ? html`<footer>${footer}</footer>`:''}`
	)
}
`;
