import html, { element} from 'encode-html-template-tag';

export default function section(title, content) {
	const s = props => element('section', props||{},
		html`<h1>${title}</h1>${content}`);
	s.title = title;
	return s;
}
