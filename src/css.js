import html from 'encode-html-template-tag';
//import escapeCss from 'css.escape';

export default function style(content, ...vars){
	if(Array.isArray(content)) {
		content = content.reduce((a, lit, ix)=>a+(vars[ix-1])+lit);
	}
	const stylesheet = html`<style>${html.safe(content)}</style>`;
	stylesheet.css = content;
	return stylesheet;
}

export function dedupeCss(dict={}) {
	return function(value) {
		if(value && value.css){
			if(!dict[value.css]) {
				return dict[value.css] = value;
			} else {
				return [];
			}
		}
		return value;
	};
}
