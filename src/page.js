import html from 'encode-html-template-tag';
import wrapper from './wrapper.js';

const style = html`
<style>
html, body {
	margin: 0;
	padding: 0;
	height: 100%;
}
* {
	box-sizing: border-box;
}
body {
	display: flex;
	flex-direction: column;
	font-family: Arial, sans-serif;
}

main.content {
	flex-grow: 1;
	margin-top: 30px;
}
main h1, main h2 {
	color: var(--theme-colour);
}
body > header {
	background: var(--theme-colour);
	color: white;
	min-height: 170px;
	display: flex;
}
body > header a:link,
body > header a:visited {
	color: white;
}

.content {
	width: 100%;
	max-width: 1200px;
	margin: 0 auto;
	padding: 10px;
}

.siteInfo{
	display: flex;
	justify-content: space-between;
}

.login{
	display: inline-block;
	background: #262626;
	padding: 3px 10.5px 4px;
	border-radius: 8px;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
	margin-top: -10px;
	font-size: 11px;
	letter-spacing: 0.5px;
}
body > footer {
	background: #F1F1F1;
	color: var(--theme-color);
}
body>header h1 {
	font-weight: normal;
	margin: 32px 0 0 0;
}
body>main h1 {
	font-weight: normal;
	letter-spacing: -0.05em;
	/*margin-top: 0;*/
}
body>header .content {
	display: flex;
	flex-direction: column;
	justify-content: space-between;
}
body>header nav {
	margin-bottom: -10px;
}
body>header nav a {
	display: inline-block;
	background: rgba(0,0,0,0.3);
	border-radius: 8px;
	border-bottom-left-radius: 0;
	border-bottom-right-radius: 0;
	padding: 5px 10.5px 5px;
	letter-spacing: 0.5px;
}
</style>
`;

/**
 * Create a basic page with header, nav, main and footer.
 * @alias module:sp-templates.page
 * @param {Object} options Page options
 * @param {String} options.title Page title
 * @param {String} options.colour Theme colour
 * @param {String|Html} body The body
 */
export default ({title, head, footer, colour, controls='', menu=[{href:'/', name:'Home'}]}, body) => wrapper(
	{title, head, style:`--theme-colour: ${colour || 'rebeccapurple'}`},
	html`
	${style}
	<header>
		<div class="content">
			<div class="siteInfo">
		  		<h1>${title}</h1>
				<div>${controls}</div>
			</div>
			<nav>
				${menu.map(({name, href})=>html`<a href="${href}">${name}</a>`)}
			</nav>
		</div>
	</header>
	<main class="content">
		${body}
	</main>
	${footer ? html`<footer>
		<div class="content">${footer}`:''}`
);
