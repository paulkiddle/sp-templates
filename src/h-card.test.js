import hCard from '../src/h-card.js';

test('Renders h-card of author', async()=>{
	expect(await hCard.author({
		url: 'http://example.com',
		name: 'Paul',
		avatar: 'img.jpg'
	}).render()).toMatchSnapshot();
});

test('Renders generic h-card', async()=>{
	expect(
		await hCard(
			{
				class: 'Test'
			},
			'Paul'
		).render()
	).toMatchSnapshot();
});
