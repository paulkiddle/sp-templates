import html, { element as tag } from 'encode-html-template-tag';

const style = html`<style>
.User {
	display: block;
	border-radius: 10px;
	padding: 5px;
	background: rgba(0,0,0,0.3);
	min-width: 100px;
	text-align: center;
}
</style>`;


const user = (props, name) => html`${style}${tag('a', { class: 'User', ...props}, name)}`;

export default user;
