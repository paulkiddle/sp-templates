import { element, flatt } from 'encode-html-template-tag';

/**
 * @module sp-templates
 */

function params(attrs, date) {
	return date ? [attrs, new Date(date)] : [{}, new Date(attrs)];
}

const createTime = formatter => {
	const time = (attrs, date) => {
		[attrs, date] = params(attrs, date);

		return element('time', { datetime: date.toISOString(), ...attrs }, formatter(date, attrs.lang));
	};

	time.published = (attrs, date) => {
		[attrs, date] = params(attrs, date);

		const { class: cls, ...rest } = attrs;

		return time({ class: flatt('dt-published', cls), itemprop: 'published', ...rest }, date);
	};

	return time;
};

const langTime = createTime((date, locale)=>date.toLocaleString(locale));

/**
 * Create an HTML element representing a date and time
 * @param {Object} options Extra attributes to add to the time component
 * @param {String} options.lang The locale to render the text in
 * @param {Date} date The datetime to use
 * @alias module:sp-templates.time
 */
const time = (options, date) => {
	[options, date] = params(options, date);

	const { locale, ...attrs } = options;

	return langTime({ lang: locale, ...attrs }, date);
};

time.published = langTime.published;

time.format = createTime;

export default time;
