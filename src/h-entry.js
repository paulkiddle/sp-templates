import html, { element, flatt } from 'encode-html-template-tag';

export default function hEntry(params, body) {
	const {class: cls, ...attrs } = params;

	const child = body(content => html`<div itemprop="content" class="e-content">
		${content}
	</div>`);

	return element(
		'article',
		{ itemscope: true, itemtype: 'http://microformats.org/wiki/h-entry', class: flatt('h-entry', cls), ...attrs },
		child
	);
}
