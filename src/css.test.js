import html from 'encode-html-template-tag';
import css, { dedupeCss } from '../src/css.js';

test('Deduplicates CSS', async()=>{
	const styles = css`a{color:red;}`;
	const s2 = css('a{color:red;}');
	const link = text => html`${styles}${s2}<a>${text}</a>`;
	const view = html`${link('One')} ${link('Two')}`;

	expect(await view.render(dedupeCss())).toMatchSnapshot();
});
