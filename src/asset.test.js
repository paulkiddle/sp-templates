import html from 'encode-html-template-tag';
import asset, { replaceAsset } from '../src/asset.js';
import { jest } from '@jest/globals';

test('Replaces asset with URL', async()=>{
	const a = asset('/tmp/fake.png');

	const getUrl = jest.fn(s=>s.location.replace('/tmp/', '/assets/'));

	const str = '';
	const view = html`<img src="${a}">${str}`;

	expect(await view.render(replaceAsset(getUrl))).toMatchSnapshot();
	expect(getUrl).toHaveBeenCalledWith(a);
});

test('Renders data URL by default', async ()=>{
	const a = asset('asset.txt', import.meta);
	const a2 = asset('asset.txt', import.meta.url);

	expect(a.location).toEqual(a2.location);

	expect(await html`${a}`.render()).toMatchSnapshot();
});
