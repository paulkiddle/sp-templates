import table, { tbody, numeric, thead } from '../src/table.js';

test('Render table', async()=>{
	expect(await table(
		[{ title:'group', group:['Zero', numeric('One')]}, ['Two', '2']],
		[
			[0, 1, 2],
			{'zero':'Z', 'one':3, '2':4}
		]
	).render()).toMatchSnapshot();

	expect(await table({id:'table'}, [], []).render()).toMatchSnapshot();
});

test('TBODY', async()=>{
	expect(await tbody({}, ['a', 'b'], [{a:1, b:2, c:3},{a:3,b:5}]).render()).toMatchSnapshot();
});

test('THEAD' , async()=>{
	expect(await thead({}, [{'title':'First'}, 'second']).render()).toMatchSnapshot();
});
