import html, { element as tag } from 'encode-html-template-tag';

export default ({ title, head, ...bodyAttrs }, ...body) => html`<!doctype HTML><html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<title>${title}</title>
${head||''}
</head>${tag('body', bodyAttrs, ...body)}`;
