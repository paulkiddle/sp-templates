import http from 'http';
import { page, time, user, section, note, dedupeCss, css} from '../src/index.js';

http.createServer(
	async (req, res) => res.end(await page({'title':'Title', menu:[
		{ name:'Home', href: '/' },
		{ name:'Website', href: 'http://example.com' },
	],
	controls: [
		user({ href:'/paul' }, 'Paul'),
		user({ }, 'Paul2')
	]
	},
	[
		'body: ',
		css`.Note { border: 1px solid red; }`,
		time({locale:'fr-FR'}, new Date),
		section('Section', 'Section body')(),
		note({ author: 'Paul' }, 'My post'),
		note({
			author: {
				name: 'Paul',
				avatar: 'https://via.placeholder.com/140x100',
				url: '#'
			},
			footer:'My footer',
			date: '2020-01-01'
		}, 'My post 2')
	]).render(dedupeCss()))
).listen(3333, ()=>{
	console.log('http://localhost:3333')
})
